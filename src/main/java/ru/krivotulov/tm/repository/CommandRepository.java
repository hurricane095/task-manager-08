package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.ICommandRepository;
import ru.krivotulov.tm.constant.ArgumentConst;
import ru.krivotulov.tm.constant.TerminalConst;
import ru.krivotulov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system info."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of terminal commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display argument list."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display command list."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO,
            ABOUT,
            VERSION,
            HELP,
            ARGUMENTS,
            COMMANDS,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
